;;;; chanviz.asd

(asdf:defsystem #:chanviz
  :serial t
  :description "Visualize data flow across integration channels."
  :author "Anton Rizov <anton.rizov@gmail.com>"
  :license "Specify license here"
  :depends-on (#:cxml)
  :components ((:file "package")
               (:file "chanviz")))

