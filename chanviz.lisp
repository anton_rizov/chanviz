;;;; chanviz.lisp
(in-package #:chanviz)

(defun blankp (s)
  (string= s ""))

(defun indent (&optional (level 2))
  (loop for i below level
        do (princ #\Space)))

;;; 
(defparameter *dispatch-table* (make-hash-table :test #'equal))

(defun register-element-processor (namespace-uri local-name function)
  (let ((table (gethash namespace-uri *dispatch-table*)))
    (unless table
      (setf table (make-hash-table :test #'equal))
      (setf (gethash namespace-uri *dispatch-table*) table))
    (setf (gethash local-name table) function)))

(defun process-element (element)
  (let* ((namespace-uri (dom:namespace-uri element))
         (table (gethash namespace-uri *dispatch-table*)))
    (if table
        (let* ((local-name (dom:local-name element))
               (fun (gethash local-name table)))
          (if fun
              (funcall fun element))))))

(defparameter *current-ns* nil)

(defun use-ns (ns)
  (setf *current-ns* ns))

(defmacro defprocessor (name (arg) &body body)
  `(register-element-processor *current-ns* ,name #'(lambda (,arg) ,@body)))

;;; 
(defparameter *nodes* nil)
(defparameter *ignore-input-nodes* nil)

(defun mark-node (node)
  (pushnew node *nodes* :test #'string=))

(defun dont-track-incoming-edges (node)
  (pushnew node *ignore-input-nodes* :test #'string=))

(defun node-member-p (node set)
  (member node set :test #'string=))

(defun edge (from to)
  (indent)
  (unless (or (blankp from)
              (blankp to))
    (mark-node from)
    (mark-node to)
    (format T "~A -> ~A;~%" from to)))

(defun edge-attributes (element from to)
  (edge (dom:get-attribute element from) (dom:get-attribute element to)))

;;; 
(defun process-child-nodes (parent)
  (loop for node across (dom:child-nodes parent)
        when (dom:element-p node)
          do (process-element node)))

;;; 
(defparameter *integration-ns* "http://www.springframework.org/schema/integration")
(defparameter *jms-ns* "http://www.springframework.org/schema/integration/jms")
(defparameter *beans-ns* "http://www.springframework.org/schema/beans")

(defparameter *input-channel* nil)
(defparameter *output-channel* nil)

(defun get-channel-default (channel default)
  (if (blankp channel)
      default
      channel))

(defun get-input-channel (x)
  (get-channel-default x *input-channel*))

(defun get-output-channel (x)
  (get-channel-default x *output-channel*))

(use-ns *beans-ns*)

(defparameter *bean* nil)

(defprocessor "bean" (bean)
  (let ((*bean* (dom:get-attribute bean "id")))
    (dont-track-incoming-edges *bean*)
    (process-child-nodes bean)))

(defprocessor "property" (p)
  (let ((ref (dom:get-attribute p "ref")))
    (when (and (node-member-p ref *nodes*)
               (not (node-member-p ref *ignore-input-nodes*)))
      (format t "~A[shape=box];~%" *bean*)
      (edge *bean* ref))))


(use-ns *integration-ns*)

(defprocessor "chain" (chain)
  (let ((*input-channel* (dom:get-attribute chain "input-channel"))
        (*output-channel* (dom:get-attribute chain "output-channel")))
    (edge *output-channel* *input-channel*)
    ;; (format t "subgraph cluster_~A {~%" (gensym))
    ;; (format t "color=blue;~%")
    (process-child-nodes chain)
    ;; (format t "}~%")
    ))

(defun defsimple-processor (name &key (from "input-channel") (to "output-channel"))
  (defprocessor name (el)
    (edge-attributes el from to)))

(defsimple-processor "transformer")
(defsimple-processor "filter")
(defsimple-processor "splitter")

(defprocessor "service-activator" (activator)
  (let ((input-channel (dom:get-attribute activator "input-channel"))
        (output-channel (dom:get-attribute activator "output-channel"))
        (ref (dom:get-attribute activator "ref")))
    (assert (not (equal "" ref)))
    (format t "~A[shape=box];~%" ref)
    (edge (get-input-channel input-channel) ref)
    (unless (equal "" output-channel)
      (edge ref output-channel))))

(defsimple-processor "bridge")

(defprocessor "object-to-json-transformer" (transformer)
  (let ((input-channel (dom:get-attribute transformer "input-channel"))
        (output-channel (dom:get-attribute transformer "output-channel"))
        (mapper (dom:get-attribute transformer "object-mapper")))
    (cond ((blankp mapper)
           (edge input-channel output-channel))
          (t
           (edge input-channel mapper)
           (edge mapper output-channel)))))

(defprocessor "router" (router)
  (let ((*input-channel* (dom:get-attribute router "input-channel")))
    (process-child-nodes router)))

(defprocessor "mapping" (mapping)
  (edge *input-channel* (dom:get-attribute mapping "channel")))

;; (defun first-non-empty-attribute (element &rest attributes)
;;   (loop for attribute in attributes
;;         for value = (dom:get-attribute element attribute)
;;         unless (blankp value) return value))

(defprocessor "outbound-channel-adapter" (adapter)
  (edge (get-input-channel (dom:get-attribute adapter "channel"))
        (dom:get-attribute adapter "ref")))

(use-ns *jms-ns*)
(defprocessor "outbound-channel-adapter" (adapter)
  (edge (get-input-channel (dom:get-attribute adapter "channel"))
        (dom:get-attribute adapter "destination")))

(defsimple-processor "message-driven-channel-adapter"
                     :from "channel"
                     :to "destination")

;;; 
(defun find-spring-files (root-directory)
  (flet ((run-find ()
           (with-output-to-string (out)
             (sb-ext::run-program "find" (list root-directory
                                               "-path" "*/src/main/resources/*.xml")
                                  :output out
                                  :search t
                                  :wait t))))
    (with-input-from-string (s (run-find))
      (loop for line = (read-line s nil)
            while line
            collect line))))

(defparameter *project-root-directory* "/home/arizov/src/plutus/")

(defparameter *input-files*
  (find-spring-files *project-root-directory*))

(defparameter *image-type* "png")
(defparameter *image* (concatenate 'string
                                   *project-root-directory*
                                   "integration."
                                   *image-type*))


;;; 
(defun process-spring-xml-1 (root)
  (let ((beans (dom:child-nodes root)))
    (assert (= 1 (length beans)))
    (process-child-nodes (aref beans 0))))

(defun process-spring-xml (file)
  ;; (format t "subgraph cluster_~A {~%" (gensym))
  ;; (format t "label=\"~A\";color=blue;~%" file)
  (process-spring-xml-1 (cxml:parse-file file (cxml-dom:make-dom-builder)))
  ;; (format t "}~%")
  )


(defun generate-dot (files)
  (with-output-to-string (*standard-output*)
    (progn
      (format t "digraph G {~@
  rankdir=LR;~%~@
  concentrate=true;~%")
      ;; files are unsorted, hence we could see a `bean' before seeing
      ;; any `channel' it uses.
      (dotimes (i 2)
        (declare (ignorable i))
        (mapc #'process-spring-xml files))
      (format t "}~%"))))

(defun run-dot (source)
  (sb-ext::run-program "dot" (list "-T" *image-type*
                                   "-o" *image*)
                       :wait t
                       :input source
                       :search t))

;;; 
(defun main ()
  (with-input-from-string (stream (generate-dot *input-files*))
    (run-dot stream)))

(progn
  (main)
  (sb-ext::run-program "xdg-open" (list *image*) :search t))
